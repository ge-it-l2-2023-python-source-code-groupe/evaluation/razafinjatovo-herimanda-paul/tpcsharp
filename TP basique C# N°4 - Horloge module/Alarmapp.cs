using System;
using System.Collections.Generic;
namespace Alarm
{



    class AlarmApp
    {
        private List<Alarm> alarmList = new List<Alarm>();

        public void Run()
        {
            int choice;

            do
            {
                DisplaySubMenu();
                choice = GetSubMenuChoice();

                switch (choice)
                {
                    case 1:
                        ViewActiveAlarms();
                        break;
                    case 2:
                        CreateAlarm();
                        break;
                    case 3:
                        Console.WriteLine("Retour au menu principal.");
                        break;
                    default:
                        Console.WriteLine("Choix non valide. Veuillez réessayer.");
                        break;
                }

            } while (choice != 3);
        }

        private void DisplaySubMenu()
        {
            Console.WriteLine("Choisissez une option :");
            Console.WriteLine("1 - Voir les a33larmes actives");
            Console.WriteLine("2 - Créer une alarme");
            Console.WriteLine("3 - Retour au menu principal");
            Console.Write("Quel est votre choix ? ");
        }

        private int GetSubMenuChoice()
        {
            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine("Entrée non valide. Veuillez entrer un nombre.");
                Console.Write("Quel est votre choix ? ");
            }
            return choice;
        }

        private void ViewActiveAlarms()
        {
            Console.WriteLine("Liste des alarmes actives :");
            foreach (var alarm in alarmList)
            {
                Console.WriteLine(alarm.ToString());
            }
        }

        private void CreateAlarm()
        {
            Console.WriteLine("Info sur la nouvelle alarme :");

            Console.Write("Donnez un nom de référence : ");
            string name = Console.ReadLine();

            Console.Write("Heure de l'alarme (hh:mn) : ");
            string time = Console.ReadLine();

            Console.Write("Date de planification (L/M/ME/J/V/S/D) : ");
            string date = Console.ReadLine();

            Console.Write("Lancer une seule fois (y/n) : ");
            bool isSingleUse = Console.ReadLine().ToLower() == "y";

            Console.Write("Périodique (y/n) : ");
            bool isPeriodic = Console.ReadLine().ToLower() == "y";

            Console.Write("Activer la sonnerie par défaut (y/n) : ");
            bool useDefaultRingtone = Console.ReadLine().ToLower() == "y";

            DateTime scheduledDate = DateTime.Parse($"{date} {time}");
        

            Alarm newAlarm = new Alarm(name, scheduledDate, isSingleUse, isPeriodic, useDefaultRingtone);
            alarmList.Add(newAlarm);

            Console.WriteLine("Votre nouvelle alarme est enregistrée :)");
        }

      

        
    }

    class Alarm
    {
        public string Name { get; set; }
        public DateTime ScheduledTime { get; set; }
        public bool IsSingleUse { get; set; }
        public bool IsPeriodic { get; set; }
        public bool UseDefaultRingtone { get; set; }

        public Alarm(string name, DateTime scheduledTime, bool isSingleUse, bool isPeriodic, bool useDefaultRingtone)
        {
            Name = name;
            ScheduledTime = scheduledTime;
            IsSingleUse = isSingleUse;
            IsPeriodic = isPeriodic;
            UseDefaultRingtone = useDefaultRingtone;
        }

        public override string ToString()
        {
            string alarmInfo = $"{Name} - {ScheduledTime:hh:mm}";
            if (IsPeriodic)
            {
                alarmInfo += $" (périodique)  {GetDaysOfWeek()}";
            }
            return alarmInfo;
        }
        private string GetDaysOfWeek()
        {
            string[] dayOfWeek = { "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam.", "Dim." };
            List<string> activeDays = new List<string>();

            for (int i = 0; i < 7; i++)
            {
                if ((ScheduledTime.DayOfWeek == DayOfWeek.Sunday && i == 6) || (i == (int)ScheduledTime.DayOfWeek - 1))
                {
                    activeDays.Add(dayOfWeek[i]);
                }
            }
            return string.Join(", ", activeDays);
        }
    }
}
using System;
using System.Security.Cryptography;
using Alarm;
using ClockApp;

namespace Tp4
{

    class Main_program
    {
        internal static void Program()
        {
            int choice;

            do
            {
                DisplayMenu();
                choice = GetChoice();

                switch (choice)
                {
                    case 1:
                        AlarmApp alarmApp = new AlarmApp();
                        alarmApp.Run();
                        break;
                    case 2:
                        ClockApp.ClockApp.RunClockApp();
                        break;

                    default:
                        break;
                }
            } while (choice != 3);

            void DisplayMenu()
            {
                Console.WriteLine("1 - Alarme");
                Console.WriteLine("2 - Horloge");
                Console.WriteLine("3 - Quitter");

            }
            int GetChoice()
            {
                int choice;
                while (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Entrée invalide");
                    Console.WriteLine("Your choice : ");
                }
                return choice;
            }



        }
    }
}
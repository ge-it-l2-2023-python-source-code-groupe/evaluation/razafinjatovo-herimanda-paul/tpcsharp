﻿using System;
using System.Threading;
using Tp1;
using Tp2;
using Tp3;
using Tp4;
namespace TpExoCsharp;


class Program
{
    static void Main(string[] args)
    {
       
        while (true){
           Console.WriteLine("-----MENU-----");
           Console.WriteLine("""
           1 - TP basique C# N°1 - Client & Compte Class
           2 - TP basique C# N°2 - Circle & Point Class
           3 - TP basique C# N°3 - Le jeu de dés
           4 - TP basique C# N°4 - Horloge module
           5 - Quitter

           """);
           Console.Write("MERCI D'ENTRER VOTRE CHOIX : ");
           string choix= Console.ReadLine();
           switch (choix)
           {
            case "1":
            Tp1.Main_program.Program();
            Console.WriteLine("Retour au menu");
            Console.ReadKey();
            break;
            case "2":
            Tp2.Main_program.Program();
              Console.WriteLine("Retour au menu");
              Console.ReadKey();
            break;
            case "3":
            Tp3.Main_program.Program();
              Console.WriteLine("Retour au menu");
              Console.ReadKey();
            break;
            case "4":
             Tp4.Main_program.Program();
               Console.WriteLine("Retour au menu");
               Console.ReadKey();
             break;
           
           }
            if(choix=="5"){
                  Console.ReadKey();
                    Console.WriteLine("Press enter to exit");
            
                break;
            }
        }
    
    }
}

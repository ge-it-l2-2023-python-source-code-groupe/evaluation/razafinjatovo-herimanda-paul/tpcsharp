using System;

namespace Point_class
{
    class Point
    {
        public double monx { get; set; }
        public double mony { get; set; }



        public Point(double monx, double mony)
        {
            this.monx = monx;
            this.mony = mony;

        }

        public void View()
        {
            Console.WriteLine($"Point ({monx},{mony})");

        }

        public void VerificationPoint(int monx, int mony, double X, double Y, double R)
        {
            double distance = Math.Sqrt(Math.Pow(monx - X, 2) + Math.Pow(mony - Y, 2));

            if (distance <= R)
            {
                Console.WriteLine($"Le point ({monx},{mony}) est à l'intérieur du cercle : ({X},{Y},{R}) ");
            }
            else
            {
                Console.WriteLine($"Le point ({monx},{mony}) n'est pas à l'intérieur du cercle  : ({X},{Y},{R}) ");
            }
        }


    }
}
using System;

namespace Cercle_class
{
    class Cercle
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double R { get; set; }

        private double pi = 3.14;

        public Cercle(double x, double y, double r)
        {
            X = x;
            Y = y;
            R = r;
        }

        public void Display()
        {
            Console.WriteLine($"Cercle ({X},{Y},{R})");
        }

        public void Perimetre()
        {
            Console.WriteLine($"Le périmètre est : {pi * 2 * R}");
        }
        public void Surface()
        {
            Console.WriteLine($"La surface est : {pi * (R * R)}");
        }
    }

}
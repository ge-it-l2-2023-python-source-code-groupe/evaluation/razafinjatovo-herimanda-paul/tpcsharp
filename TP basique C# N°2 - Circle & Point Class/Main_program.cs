using System;
using Cercle_class;
using Point_class;

namespace Tp2
{


    class Main_program
    {


        internal static void Program()
        {
            Console.Write("Donner l'abscisse du centre :");
            double x = double.Parse(Console.ReadLine());
            Console.Write("Donner l'ordonné du centre : ");
            double y = double.Parse(Console.ReadLine());
            Console.Write("Donner le rayon : ");
            double r = double.Parse(Console.ReadLine());

            //Création du cercle
            Cercle cercle = new Cercle(x, y, r);
            cercle.Display();
            cercle.Perimetre();
            cercle.Surface();

            //Input point à verifier
            Console.WriteLine("Donner un point : ");
            Console.Write("X: ");
            int monx = int.Parse(Console.ReadLine() ?? "");
            Console.Write("Y : ");
            int mony = int.Parse(Console.ReadLine() ?? "");

            //Création d'un point
            Point point = new Point(monx, mony);
            point.View();

            //Vérification de l'appartenance du point 
            point.VerificationPoint(monx, mony, x, y, r);

        }
    }
}
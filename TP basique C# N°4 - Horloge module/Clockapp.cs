using System;
using System.Collections.Generic;


namespace ClockApp
{
    class ClockApp
    {
        public static void RunClockApp(){
            ClockApp clockApp=new ClockApp();
            clockApp.Run();
        }
        private List<Clock> clockList = new List<Clock>();

        public void Run()
        {
            int choice;

            do
            {
                DisplaySubMenu();
                choice = GetSubMenuChoice();

                switch (choice)
                {
                    case 1:
                        ViewActiveClocks();
                        break;
                    case 2:
                        AddClock();
                        break;
                    case 3:
                        Console.WriteLine("Retour au menu principal.");
                        break;
                    default:
                        Console.WriteLine("Choix non valide. Veuillez réessayer.");
                        break;
                }

            } while (choice != 3);
        }

        private void DisplaySubMenu()
        {
            Console.WriteLine("Choisissez une option :");
            Console.WriteLine("1 - Voir les Horloges actives");
            Console.WriteLine("2 - Ajouter une horloge");
            Console.WriteLine("3 - Retour au menu principal");
            Console.Write("Quel est votre choix ? ");
        }

        private int GetSubMenuChoice()
        {
            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine("Entrée non valide. Veuillez entrer un nombre.");
                Console.Write("Quel est votre choix ? ");
            }
            return choice;
        }

        private void ViewActiveClocks()
        {
            Console.WriteLine("Liste des horloges :");
            foreach (var clock in clockList)
            {
                Console.WriteLine(clock.ToString());
            }

            DisplayCurrentTime(); // Afficher le temps actuel
        }

        private void DisplayCurrentTime()
        {
            // Afficher le temps actuel (peut être mis à jour en temps réel)
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss"));
            Console.WriteLine(DateTime.Now.ToString("ddd dd MMM"));
        }

        private void AddClock()
        {
            Console.WriteLine("Choisissez une ville :");
            Console.Write("[New-York, Dubaï, Hong-kong] : ");
            string city = Console.ReadLine() ?? "";

            Clock newClock = new Clock(city);
            clockList.Add(newClock);

            Console.WriteLine("Votre horloge a bien été enregistrée :)");
        }
    }

    class Clock
    {
        public string City { get; set; }
        public int TimeDifference { get; set; } // Différence par rapport à l'heure locale

        public Clock(string city)
        {
            City = city;
            // Vous pouvez ajuster les différences de temps en fonction du fuseau horaire de chaque ville
            TimeDifference = GetTimeDifference(city);
        }

        private static int GetTimeDifference(string city)
        {
            return city.ToLower() switch
            {
                "Moscou" => 3,
                "Dubaï" => 4,
                "Mexique" => -6,
                _ => 0,// Par défaut, aucune différence
            };
        }

        public override string ToString()

        {
            DateTime localTime=DateTime.UtcNow.AddHours(TimeDifference);
            string timeString= localTime.ToString("HH:mm");
            return $"{City} - {timeString} (GMT{(TimeDifference >= 0 ? "+" : "")}{TimeDifference})";
        }
    }

}